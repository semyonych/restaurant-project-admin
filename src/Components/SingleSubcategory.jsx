import React, {useState} from "react";
import {Button} from "react-bootstrap";
import {useDispatch} from "react-redux";
import {menuActions} from "../store/MenuSlice";
import SingleDish from "./SingleDish";

const styles = {
    subCatWrapper: {
        border: '1px solid #333',
        margin: '16px',
        padding: '16px'
    }
}

const SingleSubcategory = (props) => {

    const [addDish, setAddDish] = useState(false);

    const enableAddDishHandler = () => {
        setAddDish(true);
    }

    const resetAddDish = () => {
        setAddDish(false);
    }

    console.log(props.subCatName);
    console.log(props.catName);

    const dispatch = useDispatch();

    const deleteCatHandler = () => {
        if(window.confirm('Ви впевнені, що хочете видалити підкатегорію '+ props.subCatName +'? Її буде неможливо відновити після видалення.')) {
            dispatch(menuActions.removeSubCategory({catName: props.catName, subCatName: props.subCatName}));
        }
    }

    return(
        <div style={styles.subCatWrapper}>
            <div className='d-flex justify-content-between'>
                <div>
                    {props.subCatName}
                </div>
                <Button onClick={deleteCatHandler}>X</Button>
            </div>
            {props.dishes.length > 0
                ?
                props.dishes.map((dish, index) =>
                    <SingleDish
                        key={index}
                        editable={false}
                        newDish={false}
                        name={dish.name}
                        price={dish.price}
                        desc={dish.desc}
                        catName={props.catName}
                        subCatName={props.subCatName}
                        resetAddDish={resetAddDish}
                    />)
                :
                <div>Додайте ваші смачні страви тут!</div>
            }
            <div>
                {
                    addDish
                        ?
                        <div>
                            <SingleDish
                                newDish={true}
                                editable={true}
                                resetAddDish={resetAddDish}
                                catName={props.catName}
                                subCatName={props.subCatName}
                            />
                        </div>
                        :
                        <Button
                            onClick={enableAddDishHandler}
                        >
                            Додати страву
                        </Button>
                }
            </div>
        </div>
    );
}

export default SingleSubcategory;