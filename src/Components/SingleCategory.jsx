import React, {useState} from "react";
import {Button, Form} from "react-bootstrap";
import {useDispatch} from "react-redux";
import {menuActions} from "../store/MenuSlice";
import SingleSubcategory from "./SingleSubcategory";

const styles = {
    catWrapper: {
        border: '1px solid #333',
        borderRadius: '4px',
        margin: '16px 0',
        padding: '16px',
    }
}

const SingleCategory = (props) => {

    const [addCat, setAddCat] = useState(false);

    const [newCatName, setNewCatName] = useState('');

    const dispatch = useDispatch();

    const deleteCatHandler = () => {
        if(window.confirm('Ви впевнені, що хочете видалити категорію '+ props.catName +'? Її буде неможливо відновити після видалення.')) {
            dispatch(menuActions.removeCategory(props.catName));
        }
    }

    const changeAddCat = () => {
        setAddCat(!addCat);
    }
    const resetNewCatAdd = () => {
        setAddCat(!addCat);
        setNewCatName('');
    }

    const handleNewCatInputChange = (event) => {
        setNewCatName(event.target.value);
    }

    const addCategoryHandler = () => {
        if (newCatName !== ''){
            dispatch(menuActions.addSubCategory({catName: props.catName, newSubCat: newCatName}));
            setNewCatName('');
            changeAddCat();
        } else {
            window.alert('Ви не вказали назву категорії. Будь ласка вкажіть назву.');
        }
    }

    return(
        <div style={styles.catWrapper} >
            <div className='d-flex justify-content-between'>
                <div>
                    {props.catName}
                </div>
                <Button onClick={deleteCatHandler}>X</Button>
            </div>
            {props.subCats.length > 0
                ?
                props.subCats.map((cat, index) => <SingleSubcategory key={index} subCatName={cat.name} dishes={cat.dishes} catName={props.catName}/>)
                :
                <div>О ні! Категорія пуста. Наповніть її смачними стравами!</div>
            }
            <div>
                {
                    addCat
                        ?
                        <div>
                            <Form.Group className="mb-3" controlId="formBasicEmail">
                                <Form.Label>Назва категорії</Form.Label>
                                <Form.Control
                                    type="text"
                                    placeholder="Категорія"
                                    value={newCatName}
                                    onChange={handleNewCatInputChange}
                                />
                            </Form.Group>
                            <Button
                                onClick={addCategoryHandler}
                            >
                                Зберегти категорію
                            </Button>
                            <Button
                                onClick={resetNewCatAdd}
                            >
                                Скасувати
                            </Button>
                        </div>
                        :
                        <Button
                            onClick={changeAddCat}
                        >
                            Додати підкатегорію страв
                        </Button>
                }
            </div>
        </div>
    );
}

export default SingleCategory;