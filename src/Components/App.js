import React from "react";
import '../App.css';
import MenuEdit from "./MenuEdit";

function App() {
  return (
    <div className="App">
      <MenuEdit/>
    </div>
  );
}

export default App;
